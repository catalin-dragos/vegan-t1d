1. [Low carb vegan recipes](https://diabetesstrong.com/low-carb-vegan-recipes/)
*  also has macro nutrients info for each one
3. [9 Healthy and Delicious Vegan Recipes for Diabetes](https://www.vegan.io/blog/vegan-recipes-for-diabetes.html)
4. [Diabetic Vegan Recipes](http://www.eatingwell.com/recipes/18353/health-condition/diabetic/vegan/)
* some of them seem high carb
* need to check if ingredients need/can be swapped with low carb options
5. There are also meal replacement options
* such as powders 
* can be chosen to be low carb and high protein or balanced
* loads of products - [prozis romania](https://www.prozis.com/eu/en/healthy-food/high-protein-food/vegetable-protein/q/intolerance/010400006)
6. [Low carb vegan guide](https://www.dietdoctor.com/low-carb/vegan)
7. (Has eggs, but no dairy or meat) [type 1 diabetic meals in a day](https://youtu.be/ZrfrNZqefso)
