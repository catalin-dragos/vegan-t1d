## Articles containing info and anectodes relevant to the subject

1. [Healthline - Brief Description](https://www.healthline.com/health/type-1-diabetes-diet#1)

```text
Complications associated with type 1 diabetes include:
- vision problems
- high blood pressure, which increases the risk for heart attack, stroke, and poor circulation
- kidney damage
- nerve damage
- skin sores and infections, which can cause pain and may lead to tissue death
```

Animal fats and cholesterol cause heart disease [video](https://www.youtube.com/watch?v=MsFWeC-DeLo)
  
=> double down with diabetes low carb high animal fat diet

3. [Can a Vegan Diet Cure Type 1 Diabetes? (no, it can't)](https://www.vegansouls.com/can-a-vegan-diet-cure-type-1-diabetes)
4. [How Going Vegan Changed My Type 1 Diabetic Life](https://www.diabetesdailygrind.com/type1vegandiet/)
5. [Vegan Diet for Type 1 Diabetes](http://jacknorrisrd.com/vegan-diet-for-type-1-diabetes/)
6. [Ken's Engineered Type 1 Diabetes Whole Food Plant Based Low Fat Lifestyle](https://www.drcarney.com/blog/entry/ken-s-engineered-type-1-diabetes-whole-food-plant-based-low-fat-lifestyle)
7. [A Diabetic Vegan: An Interview With Adrian Kiger](https://asweetlife.org/a-diabetic-vegan-an-interview-with-adrian-kiger/)\
8. [Vegetarian and Vegan Type 1 Diabetes](https://www.diabetesselfmanagement.com/healthy-living/nutrition-exercise/vegetarian-vegan-type-1-diabetes/)
snippet
```text
Heart health

People with diabetes (type 1 or type 2) have a higher risk of heart disease and are more likely to die from cardiovascular issues (e.g., a heart attack) than people without diabetes. A healthful, plant-based diet can lower these risks by reducing LDL (bad) cholesterol levels, reducing inflammation, lowering blood pressure levels, and decreasing blockages in arteries. In fact, some research points to plant-based diets not only preventing heart disease, but possibly even reversing it.
Insulin resistance

Insulin resistance is when the cells in the body are unable to use insulin effectively. Insulin resistance is more common in those with type 2 diabetes, but people with type 1 diabetes can be insulin resistant, too. Some health experts call this “double diabetes,” and insulin resistance in someone with type 1 greatly increase the risk of heart disease. And here’s where more of a plant-forward eating plan can help. 
```

